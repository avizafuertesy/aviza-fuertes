#### INTRO

[![Captura-de-pantalla-58.png](https://i.postimg.cc/yYW0HtXZ/Captura-de-pantalla-58.png)](https://postimg.cc/qzVNcmFJ)

[![Captura-de-pantalla-59.png](https://i.postimg.cc/HxR8sst8/Captura-de-pantalla-59.png)](https://postimg.cc/Hc0k6mRT)

[![Captura-de-pantalla-60.png](https://i.postimg.cc/mDCBCLsH/Captura-de-pantalla-60.png)](https://postimg.cc/jwxprrCx)

[![Captura-de-pantalla-61.png](https://i.postimg.cc/ZYPJq5jk/Captura-de-pantalla-61.png)](https://postimg.cc/V5NxG134)

[![Captura-de-pantalla-62.png](https://i.postimg.cc/50qDf6Jn/Captura-de-pantalla-62.png)](https://postimg.cc/Q9CfbNHT)

[![Captura-de-pantalla-63.png](https://i.postimg.cc/hPGNqD51/Captura-de-pantalla-63.png)](https://postimg.cc/nM634JZX)

#### FILES

[![Captura-de-pantalla-64.png](https://i.postimg.cc/NjjX4VLM/Captura-de-pantalla-64.png)](https://postimg.cc/mtKhZmYf)

[![Captura-de-pantalla-65.png](https://i.postimg.cc/gjpX6tfG/Captura-de-pantalla-65.png)](https://postimg.cc/5YPNhg4r)

#### BRANCHES

[![Captura-de-pantalla-66.png](https://i.postimg.cc/W1GyGdFj/Captura-de-pantalla-66.png)](https://postimg.cc/yDNnBYyr)

[![Captura-de-pantalla-67.png](https://i.postimg.cc/fRssQnYz/Captura-de-pantalla-67.png)](https://postimg.cc/pps4KS04)

[![Captura-de-pantalla-68.png](https://i.postimg.cc/HLsCjwSF/Captura-de-pantalla-68.png)](https://postimg.cc/0z34Bw1C)

[![Captura-de-pantalla-69.png](https://i.postimg.cc/vBCRPpQd/Captura-de-pantalla-69.png)](https://postimg.cc/N98PFC9N)

[![Captura-de-pantalla-70.png](https://i.postimg.cc/SR9BHDsY/Captura-de-pantalla-70.png)](https://postimg.cc/RJMpH1wv)

#### MERGE

[![Captura-de-pantalla-72.png](https://i.postimg.cc/hv8C9wvs/Captura-de-pantalla-72.png)](https://postimg.cc/vxBLy0Wx)

[![Captura-de-pantalla-73.png](https://i.postimg.cc/KYLtkTRr/Captura-de-pantalla-73.png)](https://postimg.cc/f3zV1J7J)

#### INDEX

[![Captura-de-pantalla-74.png](https://i.postimg.cc/zG4cYTsn/Captura-de-pantalla-74.png)](https://postimg.cc/NK7412fM)

[![Captura-de-pantalla-75.png](https://i.postimg.cc/0NgcBPSn/Captura-de-pantalla-75.png)](https://postimg.cc/18HGgxJV)

[![Captura-de-pantalla-76.png](https://i.postimg.cc/d084MpmY/Captura-de-pantalla-76.png)](https://postimg.cc/kVgxKjp1)

[![Captura-de-pantalla-77.png](https://i.postimg.cc/6q1hLXwd/Captura-de-pantalla-77.png)](https://postimg.cc/vxf9bC4B)

[![Captura-de-pantalla-78.png](https://i.postimg.cc/rF3JDMfT/Captura-de-pantalla-78.png)](https://postimg.cc/svPS8dFH)

#### REMOTES

[![Captura-de-pantalla-79.png](https://i.postimg.cc/Jnr3fnzN/Captura-de-pantalla-79.png)](https://postimg.cc/bG52bqbr)

[![Captura-de-pantalla-80.png](https://i.postimg.cc/Y9dNMHvJ/Captura-de-pantalla-80.png)](https://postimg.cc/Q9WKJvkb)

#### CHANGING-THE-PAST

[![Captura-de-pantalla-81.png](https://i.postimg.cc/528wjvH5/Captura-de-pantalla-81.png)](https://postimg.cc/DSyWMSPm)

[![Captura-de-pantalla-82.png](https://i.postimg.cc/mZqMwVCN/Captura-de-pantalla-82.png)](https://postimg.cc/ThVp2Vkp)